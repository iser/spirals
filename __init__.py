import st3m.run
import leds
import captouch
import os

from st3m.application import Application, ApplicationContext
from math import sin, cos, pi, tau

def enum(**enums: int):
    return type('Enum', (), enums)

SType = enum(WAIT = 0, TEXT = 1, IMAGE = 2)

class Spirals(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.init_config()
        
        try:
            self.bundle_path = app_ctx.bundle_path
        except:
            self.bundle_path = "/flash/sys/apps/spirals"

        captouch.calibration_request()
        self.d_phi = pi / 180

        self.script = [
            (SType.TEXT, "  Trust\nYourself", 100),
            (SType.WAIT, "", 5000),
            (SType.TEXT, "   Align\nYourself", 100),
            (SType.WAIT, "", 5000),
            (SType.TEXT, "Believe in\n Yourself", 120),
            (SType.WAIT, "", 5000),
            (SType.IMAGE, "hypno1.png", 120),
            (SType.IMAGE, "fairydust.png", 120),
            (SType.IMAGE, "red_lady.png", 120),
            (SType.WAIT, "", 5000)
        ]


    def init_config(self):
        self.angle = 0
        self.control_1 = tau
        self.control_2 = tau
        self.show_sugg = False
        self.s_step = 0
        self.play = False
        self.p_debounce = False
        self.ms_passed = 0
        self.led_step = 0
        self.sp_r = 200
        self.sp_g = 0
        self.sp_b = 200
        self.center_x, self.center_y = 0, 0
        self.max_radius = 120
        self.segments = 10
        self.rotation_speed = 0.02


    def draw(self, ctx: Context) -> None:
        # Clear the canvas
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.IDEOGRAPHIC
        ctx.font_size = 45
        ctx.font = ctx.get_font_name(0)
        
        leds.set_all_rgb(0, 0, 0)
        for led_idx in range(0, 39, 4):
            leds.set_rgb(led_idx + self.led_step, self.sp_r, self.sp_g, self.sp_b)
        leds.update()
        
        ctx.rgb(self.sp_r, self.sp_g, self.sp_b)

        for i in range(self.segments):
            self.draw_segment(ctx, i)
            
        if self.play and self.show_sugg:
            if SType.TEXT == self.script[self.s_step][0]:
                ctx.save()
                text_offset = ctx.text_width(self.script[self.s_step][1])
                ctx.move_to((text_offset / 4) - 5, -5)
                ctx.rgb(255, 255, 255).text(self.script[self.s_step][1])
                ctx.restore()
            elif SType.IMAGE == self.script[self.s_step][0]:
                path = f"{self.bundle_path}/{self.script[self.s_step][1]}"
                ctx.image(path, -120, -120, 241, 241)


    def draw_segment(self, ctx: Context, shift_idx):
        radius = self.max_radius
        start_angle = (shift_idx / self.segments) * tau + self.angle
        end_angle = ((shift_idx + 1) / self.segments) * tau + self.angle
        
        target_x = self.max_radius * cos(end_angle)
        target_y = self.max_radius * sin(end_angle)

        control1_angle = start_angle + self.control_1
        control2_angle = end_angle + self.control_2

        control1_x = self.center_x + radius * cos(control1_angle)
        control1_y = self.center_y + radius * sin(control1_angle)
        control2_x = self.center_x + radius * cos(control2_angle)
        control2_y = self.center_y + radius * sin(control2_angle)

        ctx.move_to(self.center_x, self.center_x)
        ctx.curve_to(control1_x, control1_y, control2_x, control2_y, target_x, target_y)
        ctx.stroke()
        

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing

        self.angle += delta_ms * 0.002  # Adjust the rotation speed
        self.ms_passed += delta_ms
        self.led_step = int(self.angle) % 4
        
        if not captouch.calibration_active():
            cs = captouch.read()

            self.petal_tristate(cs, 7, 'sp_r', 0, lambda: delta_ms % 2)
            self.var_bound('sp_r', 0, 255)

            self.petal_tristate(cs, 5, 'sp_g', 30000, lambda: delta_ms % 2)
            self.var_bound('sp_g', 0, 255)

            self.petal_tristate(cs, 3, 'sp_b', 0, lambda: delta_ms % 2)
            self.var_bound('sp_b', 0, 255)

            self.petal_tristate(cs, 1, 'control_1', 0, lambda: self.d_phi * (delta_ms % 2))
            self.var_periodic('control_1', 0, tau)

            self.petal_tristate(cs, 9, 'control_2', 0, lambda: self.d_phi * (delta_ms % 2))
            self.var_periodic('control_2', 0, tau)
            
            if cs.petals[0].pressed:
                self.p_debounce = True

            if self.p_debounce and not cs.petals[0].pressed:
                self.play = not self.play
                self.p_debounce = False

            if cs.petals[0].pressed and cs.petals[4].pressed and cs.petals[6].pressed:
                self.init_config()

        if self.play:
            if SType.TEXT == self.script[self.s_step][0] or SType.IMAGE == self.script[self.s_step][0]:
                self.show_sugg = True
                if self.ms_passed > self.script[self.s_step][2]:
                    self.show_sugg = False
                    self.next_step()
            elif SType.WAIT == self.script[self.s_step][0] and self.ms_passed > self.script[self.s_step][2]:
                self.next_step()
        

    def next_step(self):
        self.ms_passed = 0
        self.s_step += 1
        if self.s_step == len(self.script):
            self.s_step = 0


    def petal_tristate(self, ct_state, petal_idx, target, break_p, step_f):
        if ct_state.petals[petal_idx].pressed:
            if ct_state.petals[petal_idx].position[0] < break_p:
                setattr(self, target, getattr(self, target) - step_f())
            else:
                setattr(self, target, getattr(self, target) + step_f())


    def var_bound(self, target, b_min, b_max):
        if getattr(self, target) > b_max:
            setattr(self, target, b_max)
        if getattr(self, target) < b_min:
            setattr(self, target, b_min)


    def var_periodic(self, target, b_min, b_max):
        if getattr(self, target) > b_max:
            setattr(self, target, b_min)
        if getattr(self, target) < b_min:
            setattr(self, target, b_max)


if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(Spirals(ApplicationContext()))

