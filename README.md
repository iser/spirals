# spirals

- Petal 0: toggle play/pause script
- Petal 1: increase/decrease inner segment angle
- Petal 9: increase/decrease outer segment angle

- Petal 7: increase/decrease red color
- Petal 5: increase/decrease green color
- Petal 3: increase/decrease blue color
